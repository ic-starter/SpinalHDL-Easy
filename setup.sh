#!/usr/bin/env bash
#
# for ubuntu 18+, ubuntu 20+/latest is recommendded
#
# Usage:
# 1. perpare sbt package 
#   - scp/cp sbt-scala-spinalhdl-deps.tar.bz2 to /tmp
#   - wget/cp sbt-1.4.5.deb to /tmp
# 2. run auto setup
#   - $ chmod +x setup.sh
#   - $ sudo ./setup.sh

# setup apt source to aliyun/mirrors
sed -i 's/archive.ubuntu.com/mirrors.aliyun.com/' /etc/apt/sources.list
sed -i 's/security.ubuntu.com/mirrors.aliyun.com/' /etc/apt/sources.list
apt update

# install requirments tools
# interactive is needed for openjdk-8-jdk installation
apt install -y --fix-missing vim python3 python3-pip bison flex openssh-client wget \
	openjdk-8-jdk git verilator iverilog

# prepare sbt adn scala/spinalhdl dependencies
#wget https://dl.bintray.com/sbt/debian/sbt-1.4.5.deb
dpkg -i ./extra/sbt-1.4.5.deb

# install riscv toolchain
if [[ -f "extra/riscv64-unknown-elf-gcc-8.3.0-"*".tar.gz" ]]; then
  tar -xzvf ./extra/riscv64-unknown-elf-gcc-8.3.0-*.tar.gz
  mv riscv64-unknown-elf-gcc-8.3.0-* /opt/riscv
  echo "export PATH=$PATH:/opt/riscv/bin" >> ~/.bash_aliases
  source ~/.bashrc
fi

tar -xjvf ./extra/sbt-scala-spinalhdl-deps.tar.bz2
mkdir -p ~/.cache/couriser
cp -rf sbt-scala-spinalhdl-deps/.sbt ~/
cp -rf sbt-scala-spinalhdl-deps/.ivy2 ~/
cp -rf sbt-scala-spinalhdl-deps/coursier ~/.cache

rm -rf sbt-scala-spinalhdl-deps

# pull/run spinalhdl project for test
mkdir -p ~/projects/spinalhdl
cd ~/projects/spinalhdl
#git clone https://gitee.com/ic-starter/SpinalTemplateSbt.git
git clone --recursive https://gitee.com/ic-starter/SpinalTemplateSbtDependencies.git
cd SpinalTemplateSbtDependencies && sbt run
