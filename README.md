# SpinalHDL-Easy

  To build a development environment for easy way of SpinalHDL.

  By provide an offline dependencies package, SpinalHDL works without Internet! :scream: 

  Two type of implementations provided in this repo:
  - Directly installed from Ubuntu (latest 20+ is recommended)
  - A docker image built from ubuntu:latest

  Note: building procedures need internet.  I have modify the repo sources to 
        mirrors.aliyun, so there should no big problem. 

## Recommended OS

Ubuntu 18/20 - Why?

- 1. Ubuntu support more latest packages
- 2. Verilator/iverilog supported (Ubuntu 20+ is recommended)
- 3. riscv toolchain ( tools from SiFive is recommended)
- 4. Python3/pip3
- 5. sbt easy 


Details for why Ubuntu:

- SpinalHDL depends on verilator for simulation，verilator( > 3.9)
  which can be installed directly by apt-get

- For VexRiscv Dev, Toolchain from SiFive is recommended, which support both ubuntu/centos.
  SiFive's toolchain support more -mabi/-march types, target for rv32e/a/m/c, rv32g,  rv64g varies.
  (so, it's can be target for sifive cores, picorv32, lowrisc/ibex and wujian100_open)

- Easy sbt and openjdk-8/11-jdk installation


## Easy-Way Usage

 **Firstly** , download sbt dependencies：

```
https://pan.baidu.com/s/1AqfIWuBucDPk5dxHvLSI9w 
pass: ocad 

```

 **Second** , pull this repo:

`git clone https://gitee.com/ic-starter/SpinalHDL-Easy`

 **Third** , copy sbt-1.4.5, toolchain and dependencies to extra directory inside of our repo.

```
cd SpinalHDL-Easy
cp sbt-1.4.5.deb sbt-scala-spinalhdl-deps.tar.bz2 extra 
cp riscv64-unknown-elf-gcc-8.3.0-*.tar.gz extra
```

 **Forth** , for native ubuntu or WSL/ubuntu user:

```
$ chmod +x setup.sh
$ sudo ./setup.sh
```

for pure windows user, "Docker for desktop" is recommended:

```
PS > cd SpinalHDL-Easy
PS > docker build -t spinal-dev:latest .
PS > docker run -it spinal-dev
```


## Test

Inside of our new born environment:

```
git clone http://gitee.com/ic-starter/SpinalTemplateSbt
cd SpinalTemplateSbt
sbt run
```

## Important Notes:
To avoid additional dependencies for your project, make sure:

 _sbt-1.4.5/scala-2.11.12/SpinalHDL-1.4.0_ 

in your project configuration:

```
./build.sbt
./project/build.properties
```

## Additional Information

spinalHDL only support a certain version of scala. 
After kinds of test, following configuration gets more stable results. 

```
Scala 2.11.12 + SpinalHDL 1.4.0
```

If your project do not relay on VexRiscv, you can select more recent SpinalHDL, e.g, 1.4.2.
But SpinalHDL-1.4.0 is only supported in our offline dependence package.
A simple sbt project profile like this：

```
# build.sbt:
# https://gitee.com/ic-starter/SpinalTemplateSbt
name := "SpinalTemplateSbt"
version := "1.0"
scalaVersion := "2.11.12"
val spinalVersion = "1.4.0"   # or "1.4.2" if project w/o vexRiscv

libraryDependencies ++= Seq(
    "com.github.spinalhdl" % "spinalhdl-core_2.11" % spinalVersion,
    "com.github.spinalhdl" % "spinalhdl-lib_2.11" % spinalVersion,
    compilerPlugin("com.github.spinalhdl" % "spinalhdl-idsl-plugin_2.11" %s spinalVersion)
)

fork := true
# EclipseKeys.withSource := true
```

```
# project/build.properties 
sbt.version=1.4.5
```

If project relay on VexRiscv, sbt profile should be like this：
```
# build.sbt
# https://gitee.com/ic-starter/SpinalTemplateSbtDependencies
val spinalVersion = "1.4.0"   # must be "1.4.0" for VexRiscv 

lazy val root = (project in file("."))
  .settings(
    inThisBuild(List(
      organization := "com.github.spinalhdl",
      scalaVersion := "2.11.12",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "superproject",
    libraryDependencies ++= Seq(
      "com.github.spinalhdl" % "spinalhdl-core_2.11" % spinalVersion,
      "com.github.spinalhdl" % "spinalhdl-lib_2.11" % spinalVersion,
      compilerPlugin("com.github.spinalhdl" % "spinalhdl-idsl-plugin_2.11" % spinalVersion)
    )
  ).dependsOn(vexRiscv)

//For dependancies localy on your computer : 
lazy val vexRiscv = RootProject(file("./ext/VexRiscv"))

//For dependancies on a git : 
//lazy val vexRiscv = RootProject(uri("git://gitee.com/ic-starter/VexRiscv.git")) 

//For dependancies on a git with a specific commit : 
//lazy val vexRiscv = RootProject(uri("git://gitee.com/ic-starter/VexRiscv.git#commitHash")) 

fork := true
```

```
# project/build.properties
sbt.version=1.4.5
```
