FROM ubuntu:latest

MAINTAINER curliph@gmail.com

WORKDIR /

COPY ./extra/* ./tools/* /tmp/

RUN sed -i 's/archive.ubuntu.com/mirrors.aliyun.com/' /etc/apt/sources.list && \
	sed -i 's/^.*security/#&/' /etc/apt/sources.list && \
	apt update && apt install -y --fix-missing vim python3 python3-pip \
	bison flex openssh-client wget git verilator iverilog && \
	DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends --fix-missing openjdk-8-jdk && \
	dpkg -i /tmp/sbt-1.4.5.deb && \
	tar -xjvf /tmp/sbt-scala-spinalhdl-deps.tar.bz2 && \
	mkdir -p ~/.cache/couriser && \
	cp -rf sbt-scala-spinalhdl-deps/.sbt ~/ && \
	cp -rf sbt-scala-spinalhdl-deps/.ivy2 ~/ && \
	cp -rf sbt-scala-spinalhdl-deps/coursier ~/.cache && \
	mv /tmp/fvi /usr/local/bin && \
	mv /tmp/sbt-init /usr/local/bin && \
	mv /tmp/sbt-spinal /usr/local/bin && \
	rm -rf sbt-scala-spinalhdl-deps && \
	rm -rf /tmp/sbt-scala-spinalhdl-deps.tar.bz2 && \
	mkdir -p ~/projects/spinalhdl && cd ~/projects/spinalhdl && \
	git clone --recursive https://gitee.com/ic-starter/SpinalTemplateSbtDependencies

WORKDIR /root/projects

CMD ["/bin/bash"]
